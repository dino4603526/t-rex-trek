extends Node

# Preload obstacles 
var rock = preload ("res://rock.tscn")
var moving_cactus = preload("res://moving_cactus.tscn")
var area_2d = preload("res://area_2d.tscn")
var obstacle_types := [rock, moving_cactus, area_2d]
var obstacles : Array = []

# Game variables
const DINO_START_POS = Vector2i(56, 485)
const CAM_START_POS = Vector2i(576, 324)
var score : int
const SCORE_MODIFIER : int = 10
var speed : float
const START_SPEED : float = 10.9
const MAX_SPEED : int = 25
var screen_size : Vector2i
var ground_height : int
var game_running : bool
var last_obs


# Called when the node enters the scene tree for the first time.
func _ready():
	screen_size = get_window().size
	ground_height = $Ground.get_node("Sprite2D").texture.get_height()
	new_game()

func new_game():
	# Check if nodes are not null
	score = 0
	if $dino == null:
		print("Error: $dino is null")
	if $Camera2D == null:
		print("Error: $Camera2D is null")
	if $Ground == null:
		print("Error: $Ground is null")

	# Reset the nodes
	if $dino != null:
		$dino.position = DINO_START_POS
		$dino.velocity = Vector2(0, 0)
	else:
		print("Cannot set $dino.position because $dino is null")
		
	if $Camera2D != null:
		$Camera2D.position = CAM_START_POS
	else:
		print("Cannot set $Camera2D.position because $Camera2D is null")
		
	if $Ground != null:
		$Ground.position = Vector2i(0, 0)
	else:
		print("Cannot set $Ground.position because $Ground is null")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if game_running:
		speed = START_SPEED
		if $dino != null:
			$dino.position.x += speed
		else:
			print("Cannot update $dino.position because $dino is null")
			
		if $Camera2D != null:
			$Camera2D.position.x += speed
		else:
			print("Cannot update $Camera2D.position because $Camera2D is null")
		generate_obs()
		score += speed
		show_score()
		# Update the ground position
		if $Camera2D != null and $Ground != null:
			if $Camera2D.position.x - $Ground.position.x > screen_size.x * 1.5:
				$Ground.position.x += screen_size.x
		else:
			print("Cannot update $Ground.position because $Camera2D or $Ground is null")
	else:
		if Input.is_action_pressed("ui_accept"):
			game_running = true 

func generate_obs():
	# Generate ground obstacles
	if obstacles.is_empty() or last_obs.position.x < score + randi_range(300, 500):
		var obs_type = obstacle_types[randi() % obstacle_types.size()]
		var max_obs = 3
		for i in range(randi() % max_obs + 1):
			var obs = obs_type.instantiate()
			if obs != null:
				var sprite_node = obs.get_node("Sprite2D")
				if sprite_node != null:
					var obs_height = sprite_node.texture.get_height()
					var obs_scale = sprite_node.scale
					var obs_x : int = screen_size.x + score + 100
					var obs_y : int = screen_size.y - ground_height - (obs_height * obs_scale.y / 2) + 5
					last_obs = obs
					add_obs(obs, obs_x, obs_y)
				else:
					print("Error: Sprite2D node is null in obstacle")
			else:
				print("Error: Failed to instantiate obstacle")

func add_obs(obs, x, y):
	obs.position = Vector2i(x, y)
	add_child(obs)
	obstacles.append(obs)

func show_score():
	$HUD.get_node("Scorelabel").text = "SCORE : " + str(score / SCORE_MODIFIER)
