extends Node2D

@export var speed: float = 300.0  # Speed at which the wolf moves
@onready var wolf_sprite: AnimatedSprite2D = $AnimatedSprite2D

# Size of the wolf sprite (set this to your sprite's width and height)
var wolf_width: float = 64.0
var wolf_height: float = 64.0

func _ready() -> void:
	# Ensure the wolf sprite is playing the correct animation
	wolf_sprite.play("run")  # Ensure you have a "run" animation set up

func _process(delta: float) -> void:
	# Move the wolf to the left
	position.x -= speed * delta

	# Check if the wolf has moved out of the viewport
	var viewport_size = get_viewport().size

	# Check if the wolf's right edge is less than 0
	if position.x + wolf_width < 0:
		queue_free()  # Remove the wolf from the scene
