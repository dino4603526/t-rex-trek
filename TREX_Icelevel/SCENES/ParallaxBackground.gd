extends ParallaxBackground

@export var speed: float = 200.0  # Adjust the speed as needed

func _ready() -> void:
	pass

func _process(delta: float) -> void:
	for layer in get_children():
		if layer is ParallaxLayer:
			var parallax_layer = layer as ParallaxLayer
			parallax_layer.motion_offset.x -= speed * delta
