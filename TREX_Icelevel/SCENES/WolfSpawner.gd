extends Node2D

@export var wolf_scene: PackedScene
@export var min_spawn_interval: float = 3.0  # Minimum time in seconds between spawns
@export var max_spawn_interval: float = 5.5  # Maximum time in seconds between spawns
var time_since_last_spawn: float = 0.0
var spawn_interval: float = 2.3  # Initial spawn interval

# Hardcoded y-coordinate for the wolves to spawn at the same horizontal level as dino
@export var wolf_spawn_y: float = 590.0

func _ready() -> void:
	# Initialize any necessary settings
	set_random_spawn_interval()

func _process(delta: float) -> void:
	time_since_last_spawn += delta

	if time_since_last_spawn >= spawn_interval:
		spawn_wolf()
		time_since_last_spawn = 0.0
		set_random_spawn_interval()

func spawn_wolf() -> void:
	if wolf_scene:
		var wolf_instance = wolf_scene.instantiate()
		# Set the y-coordinate to the hardcoded value
		var spawn_position = Vector2(get_viewport().size.x, wolf_spawn_y)
		wolf_instance.position = spawn_position
		add_child(wolf_instance)

func set_random_spawn_interval() -> void:
	# Randomize the spawn interval between min_spawn_interval and max_spawn_interval
	spawn_interval = randf_range(min_spawn_interval, max_spawn_interval)
