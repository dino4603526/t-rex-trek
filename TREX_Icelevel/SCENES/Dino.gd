extends CharacterBody2D

@export var jump_speed: float = -370.0
@export var gravity: float = 420.0
@export var run_speed: float = 0.0  # Default running speed

@onready var animated_sprite: AnimatedSprite2D = $AnimatedSprite2D

func _ready() -> void:
	if animated_sprite:
		animated_sprite.play("idle")

func _process(delta: float) -> void:
	handle_input()
	apply_gravity(delta)
	move_and_slide()
	update_animation()

func handle_input() -> void:
	$CollisionShape2D.disabled = false
	if Input.is_action_just_pressed("jump") and is_on_floor():
		velocity.y = jump_speed
	
	elif Input.is_action_pressed("duck"):
		# Handle ducking - ensure the sprite animation and collision shape are adjusted
		$CollisionShape2D.disabled = true
		animated_sprite.play("duck")
		# Adjust run_speed or any other properties for ducking if necessary
		run_speed = 0  # Example: stop running while ducking
	
	else:
		# Normal running logic
		run_speed = 0.0  # Set to default running speed
		if animated_sprite.animation != "idle":
			animated_sprite.play("idle")

	# Set velocity.x to run_speed when not ducking
	velocity.x = run_speed

func apply_gravity(delta: float) -> void:
	if not is_on_floor():
		velocity.y += gravity * delta

func update_animation() -> void:
	if is_on_floor():
		if velocity.x != 0 and animated_sprite.animation != "idle":
			animated_sprite.play("idle")
		elif velocity.x == 0 and animated_sprite.animation != "idle":
			animated_sprite.play("idle")
	else:
		if animated_sprite.animation != "jump":
			animated_sprite.play("jump")
		elif animated_sprite.animation != "duck":
			animated_sprite.play("duck")
