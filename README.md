---
----
----
TREX TREK
----
----
Welcome to TREX TREK—an exciting evolution of the classic Chrome Dino game, now featuring enriched narrative depth, engaging themes, and enhanced gameplay! Explore this README to learn more about the game’s features, themes, and the technology behind its creation.

----
Overview
----

TREX TREK transforms the classic Chrome Dino game into a thrilling adventure with a rich storyline and thematic worlds. Players guide their dinosaur through various environments, each offering unique challenges and obstacles.

----
----
Game Features
----
----

----
Narrative Depth
----

Embark on a story-driven adventure as you guide the Dino through diverse worlds. Each theme introduces unique narrative elements that deepen the gameplay experience and immerse you in the game's storyline.

----
Themes
----
    -> Desert: Conquer the arid landscapes of the desert, complete with sandy dunes, cacti, and hidden challenges. Beware of dust storms and quicksand!

    -> Ice: Navigate the icy terrains of the frozen world, where slippery surfaces, snow-covered obstacles, and frosty hazards await. The cold introduces new challenges and environmental effects.

    -> Volcano: Face the heat of the volcanic realm, where molten lava, eruptions, and volcanic rocks create intense obstacles. Stay sharp as the environment heats up!

----
Enhanced Gameplay
----

    -> Increased Obstacles: The game features a wider variety of obstacles across all themes, adding to the challenge and excitement as you navigate through each level.

----
Technology Used
----

    -> Game Engine: Godot Engine

        Godot provides a powerful platform for game development, enabling us to create a seamless and engaging gameplay experience.

    -> 2D Sprite Management: AnimatedSprite2D

        Used for smooth and dynamic animations of the Dino and various in-game elements.

    -> Parallax Backgrounds: ParallaxBackground

        Creates a sense of depth and motion in the game’s environments with dynamic scrolling backgrounds.

    -> Procedural Generation: Implemented to create varied and unpredictable obstacle patterns, ensuring a unique experience in every playthrough.

    -> Collision Detection and Physics: Utilized to manage interactions between the Dino and obstacles, providing a responsive and challenging gameplay experience.


----
----
Gameplay
----
----

Experience the thrill of TREX TREK as you traverse through the Desert, Ice, and Volcano themes. Each level is designed to test your reflexes and adaptability with an array of new obstacles and challenges.



Enjoy your adventure in TREX TREK and test your skills across diverse and challenging environments!

